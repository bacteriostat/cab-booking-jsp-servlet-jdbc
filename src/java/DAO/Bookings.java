
package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import Models.BookingModel;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Bookings {
    
    private Connection con;
    
    public Bookings(){
        try {  
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
            con = DriverManager.getConnection(
                    "jdbc:derby://localhost:1527/CabBookingDB",
                    "root",
                    "root"
            );
        } catch (SQLException e) {
            System.out.println("SQL Exception: "+ e.toString());
        } catch (ClassNotFoundException cE) {
            System.out.println("Class Not Found Exception: "+ cE.toString());
        }
    }
    
    public ArrayList<BookingModel> getBookings(String uname){
        ArrayList<BookingModel> bookingData=new ArrayList<>();
        
        try { 
            String query="";
            if(uname.equals("admin")){
                query+= "select * from bookings";
            }else{
                query+= "select * from bookings where username=\'"+uname+"\'";
            }
            Statement stmt= con.createStatement();
            ResultSet rs= stmt.executeQuery(query);
            while(rs.next()){
                BookingModel tempData=new BookingModel();
                tempData.booking_id=rs.getString(1);
                tempData.username=rs.getString(2);
                tempData.pick_up_location=rs.getString(3);
                tempData.drop_location=rs.getString(4);
                tempData.date=rs.getString(5);
                tempData.pick_up_time=rs.getString(6);
                tempData.status=rs.getString(7);
                bookingData.add(tempData);
            }
            con.close();
        } catch (SQLException e) {
            System.out.println("SQL Exception: "+ e.toString());
        }
        
        return bookingData;
    }
}
