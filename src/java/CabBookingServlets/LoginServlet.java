/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CabBookingServlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author daemone
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {
    
    private Connection con;
    
    @Override
    public void init() throws ServletException {
        ServletContext context = getServletContext();
        try {  
            Class.forName(context.getInitParameter("driver"));
            con = DriverManager.getConnection(
                    context.getInitParameter("database"),
                    context.getInitParameter("user"),
                    context.getInitParameter("password")
            );
        } catch (SQLException e) {
            System.out.println("SQL Exception: "+ e.toString());
        } catch (ClassNotFoundException cE) {
            System.out.println("Class Not Found Exception: "+ cE.toString());
        }
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String uname = request.getParameter("username");
        String passwd = request.getParameter("password");
        try {  
            String query= "select * from users where username=\'"+uname+"\' AND password=\'"+passwd+"\'";
            Statement stmt= con.createStatement();
            ResultSet rs= stmt.executeQuery(
                    query
            );
            if(rs.next()){
               HttpSession session = request.getSession();
               session.setAttribute("username", uname);
               response.sendRedirect("homepage.jsp");
            }else{
                response.setContentType("text/html");
                PrintWriter out=response.getWriter();
                out.println("<div style=\"text-align: center; color: red;\">");
                out.println("Invalid username or password!");
                out.println("</div>");
                RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
                rd.include(request, response);
            }
        } catch (SQLException e) {
            System.out.println("SQL Exception: "+ e.toString());
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
