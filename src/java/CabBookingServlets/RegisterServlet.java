/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CabBookingServlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author daemone
 */
public class RegisterServlet extends HttpServlet {
    
    private Connection con;
    
    @Override
    public void init() throws ServletException {
        ServletContext context = getServletContext();
        try {  
            Class.forName(context.getInitParameter("driver"));
            con = DriverManager.getConnection(
                    context.getInitParameter("database"),
                    context.getInitParameter("user"),
                    context.getInitParameter("password")
            );
        } catch (SQLException e) {
            System.out.println("SQL Exception: "+ e.toString());
        } catch (ClassNotFoundException cE) {
            System.out.println("Class Not Found Exception: "+ cE.toString());
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {  
            String query= "insert into users values(?,?,?,?)";
            PreparedStatement pstmt= con.prepareStatement(query);
            pstmt.setString(1, request.getParameter("username"));
            pstmt.setString(2, request.getParameter("password"));
            pstmt.setString(3, request.getParameter("city"));
            pstmt.setString(4, request.getParameter("name"));
            if(pstmt.executeUpdate()==1){
                request.getSession().setAttribute("username", request.getParameter("username"));
                response.sendRedirect("homepage.jsp");
            };           
        } catch (SQLIntegrityConstraintViolationException e) {
            response.setContentType("text/html");
            PrintWriter out=response.getWriter();
            out.println("<div style=\"text-align: center; color: red;\">");
            out.println("Username not available!");
            out.println("</div>");
            RequestDispatcher rd=request.getRequestDispatcher("register.jsp");
            rd.include(request, response);
        } catch (SQLException e){
        
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Register Servlet";
    }// </editor-fold>

}
