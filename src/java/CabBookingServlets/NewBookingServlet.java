package CabBookingServlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NewBookingServlet extends HttpServlet {
    
    private Connection con;
    
    @Override
    public void init() throws ServletException {
        ServletContext context = getServletContext();
        try {  
            Class.forName(context.getInitParameter("driver"));
            con = DriverManager.getConnection(
                    context.getInitParameter("database"),
                    context.getInitParameter("user"),
                    context.getInitParameter("password")
            );
        } catch (SQLException e) {
            System.out.println("SQL Exception: "+ e.toString());
        } catch (ClassNotFoundException cE) {
            System.out.println("Class Not Found Exception: "+ cE.toString());
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {  
            String query= "insert into bookings values(?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement pstmt= con.prepareStatement(query);
            pstmt.setString(1, ((int)(Math.random()*100000))+"");
            pstmt.setString(2, request.getSession().getAttribute("username")+"");
            pstmt.setString(3, request.getParameter("pick_up_location"));
            pstmt.setString(4, request.getParameter("drop_location"));
            pstmt.setString(5, request.getParameter("date"));
            pstmt.setString(6, request.getParameter("pick_up_time"));
            pstmt.setString(7, "pending");

            if(pstmt.executeUpdate()==1){
                response.sendRedirect("homepage.jsp");
            };           
        } catch (SQLIntegrityConstraintViolationException e) {
            response.setContentType("text/html");
            PrintWriter out=response.getWriter();
            out.println("<div style=\"text-align: center; color: red;\">");
            out.println("Something went wrong. Please try again!");
            out.println("</div>");
            RequestDispatcher rd=request.getRequestDispatcher("newBooking.jsp");
            rd.include(request, response);
        } catch (SQLException e){
        
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
