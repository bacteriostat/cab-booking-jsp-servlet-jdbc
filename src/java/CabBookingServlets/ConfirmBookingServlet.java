package CabBookingServlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ConfirmBookingServlet", urlPatterns = {"/confirm"})
public class ConfirmBookingServlet extends HttpServlet {
    
    private Connection con;
    
    @Override
    public void init() throws ServletException {
        ServletContext context = getServletContext();
        try {  
            Class.forName(context.getInitParameter("driver"));
            con = DriverManager.getConnection(
                    context.getInitParameter("database"),
                    context.getInitParameter("user"),
                    context.getInitParameter("password")
            );
        } catch (SQLException e) {
            System.out.println("SQL Exception: "+ e.toString());
        } catch (ClassNotFoundException cE) {
            System.out.println("Class Not Found Exception: "+ cE.toString());
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out=response.getWriter();
        try {  
            String query= "update bookings set status = 'confirmed' where booking_id=?";
            PreparedStatement pstmt= con.prepareStatement(query);
            pstmt.setString(1, request.getParameter("id"));

            if(pstmt.executeUpdate()==1){
                out.println("<div style=\"text-align: center; color: green;\">");
                out.println("Booking "+request.getParameter("id")+" confirmed!");
                out.println("</div>");
                RequestDispatcher rd=request.getRequestDispatcher("adminHome.jsp");
                rd.include(request, response);
            };           
        } catch (SQLIntegrityConstraintViolationException e) {
            out.println("<div style=\"text-align: center; color: red;\">");
            out.println("Something went wrong. Please try again!");
            out.println("</div>");
            RequestDispatcher rd=request.getRequestDispatcher("adminHome.jsp");
            rd.include(request, response);
        } catch (SQLException e){
            System.out.println(e+"");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
