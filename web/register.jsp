<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register</title>
    </head>
    <style>
        body{
             background: url("assets/splash.jpg");
             background-repeat: no-repeat;
             background-attachment: fixed;
             background-position: center; 
             background-size: 60vh; 
        }
    </style>
    <body>
        <h2 style="position: absolute; margin: 50px;">Register</h2>
        <h3 style="position: absolute; margin-left: 50px; margin-top: 90px;">Enter details to continue!</h3>
        <form action="register-user" method="post" style="position: absolute; margin-left: 50px;margin-top: 150px;">
            <input type="text" name="name" placeholder="Name">
            <br><br>
            <input type="text" name="city" placeholder="City">
            <br><br>
            <input type="text" name="username" placeholder="Username">
            <br><br>
            <input type="password" name="password" placeholder="Password">
            <br><br>
            <input type="submit" value="Sign Up">
        </form>
    </body>
</html>
