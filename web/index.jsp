<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Book your cab!</title>
    </head>
    <style>
        body{
             background: url("assets/splash.jpg");
             background-repeat: no-repeat;
             background-attachment: fixed;
             background-position: center; 
             background-size: 60vh; 
        }
    </style> 
    <body>
        <%
            try{
                if(session.getAttribute("username")!=null){
                    response.sendRedirect("homepage.jsp");
                }
            }catch(Exception e){}
            //            session.removeAttribute("username");
        %>
        <h2 style="position: absolute; margin: 50px;">Cab Booking Service</h2>
        <h3 style="position: absolute; margin-left: 50px; margin-top: 90px;">Login to continue</h3>
        <div style="position: absolute; margin-left: 50px;margin-top: 120px;">
            <a href="admin.jsp">Administrator login</a>
        </div>
        <form action="login" method="post" style="position: absolute; margin-left: 50px;margin-top: 170px;">
            <input type="text" name="username" placeholder="Username">
            <br><br>
            <input type="password" name="password" placeholder="Password">
            <br><br>
            <input type="submit" value="Log In">
        </form>
        <div style="position: absolute; margin-left: 170px;margin-top: 280px;">
            <a href="register.jsp">Register</a>
        </div>
    </body>
</html>
