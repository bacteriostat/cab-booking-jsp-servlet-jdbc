<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New Ride</title>
    </head>
    <style>
        body{
             background: url("assets/splash.jpg");
             background-repeat: no-repeat;
             background-attachment: fixed;
             background-position: center; 
             background-size: 60vh; 
        }
    </style>
    <body>
        <h1 style="position: absolute; margin: 50px;">Fill in details</h1>
        <form action="book" method="post" style="position: absolute; margin-left: 50px;margin-top: 110px;">
            <input type="text" name="pick_up_location" placeholder="Pick-up location">
            <br><br>
            <input type="text" name="drop_location" placeholder="Drop location">
            <br><br>
            <input type="text" name="date" placeholder="Date">
            <br><br>
            <input type="text" name="pick_up_time" placeholder="Pick-up time">
            <br><br>
            <input type="submit" value="Book Ride">
        </form>
        <div style="position: absolute; margin-left: 190px;margin-top: 325px;">
            <a href="homepage.jsp">Back</a>
        </div>
    </body>
</html>
