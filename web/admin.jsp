<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin Login</title>
    </head>
    <style>
        body{
             background: url("assets/admin.jpg");
             background-repeat: no-repeat;
             background-attachment: fixed;
             background-position: center; 
             background-size: 20vh; 
        }
    </style>
    <body>
        <h3 style="position: absolute; margin-left: 50px; margin-top: 50px;">Login to continue</h3>
        <form action="admin-login" method="post" style="position: absolute; margin-left: 50px;margin-top: 100px;">
            <input type="text" name="username" placeholder="Username">
            <br><br>
            <input type="password" name="password" placeholder="Password">
            <br><br>
            <input type="submit" value="Log In">
        </form>
    </body>
</html>
