<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="DAO.Bookings" %>
<%@page import="java.util.ArrayList" %> 
<%@page import="Models.BookingModel" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
    </head>
    <style>
        td{
            width: 20%;
        }
        .list-hr{
            color: grey;
        }
    </style>
    <body>
        <!--        Declarations    -->
        <%!   
            String uname;
            ArrayList<BookingModel> bookingList;
            
            private void getBookingData(){  
                  Bookings bookingDao=new Bookings();
                  bookingList=bookingDao.getBookings(uname);
            }  
        %> 
        <!--        Set the username and get booking data  -->
        <%
            uname=(String) (session.getAttribute("username"));
            getBookingData();
        %>
        <!--        Greeting and logout button   -->
        <h1>Welcome <%=uname%>!</h1>
        <!--        Navigation      -->
        <a href="${pageContext.request.contextPath}/newBooking.jsp"><button>Book a ride</button></a>
        <a href="${pageContext.request.contextPath}/logout"><button>Log Out</button></a>
        
        <!--        Display Booking List    -->
        <h2>Past Bookings</h2>
        <table>
            <tr>
		<td>
                    <p>BOOKING ID</p>
		</td>
		<td>
                    <p>PICK-UP LOCATION</p>
		</td>
		<td>
                    <p>DROP LOCATION</p>
		</td>
		<td>
                    <p>DATE</p>
		</td>
                <td>
                    <p>PICK-UP TIME</p>
		</td>
                <td>
                    <p>STATUS</p>
		</td>
            </tr>
        </table>
        <hr style="border-width: 3px; color: yellow;">
        <%
            if(bookingList.size()!=0){
                for(int i=0;i<bookingList.size();i++){
        %>
        <!--        List Element    -->
        <table>
            <tr>
		<td>
                    <p><%=bookingList.get(i).booking_id%></p>
		</td>
		<td>
			<p><%=bookingList.get(i).pick_up_location%> </p>
		</td>
		<td>
			<p><%=bookingList.get(i).drop_location%> </p>
		</td>
		<td>
			<p><%=bookingList.get(i).date%> </p>
		</td>
                <td>
			<p><%=bookingList.get(i).pick_up_time%> </p>
		</td>
                <td>
			<p><%=bookingList.get(i).status%> </p>
		</td>
            </tr>
        </table>
        <hr class="list-hr">
        <!--        List Element Ends   -->
        <%
                }
            }else{
        %>
        <p style="text-align: center;">No items</p>
        <% } %>
    </body>
</html>
